#!/bin/bash

SIRIUS="../source/siriusd"
SIRIUS_PIPE="/tmp/log"
LOGS="/tmp/outlog."
COMPRESSED_DIR="/tmp/compressed"
COMPRESSED_FILE="clog"

function print_passed {
	echo -e "\e[42m[PASSED]\e[0m $1"
}

function print_failed {
	echo -e "\e[41m[FAILED]\e[0m $1"
}

# run before each test case
function setup {
	# mkdir -p $SIRIUS_LOG_DIR
	$SIRIUS -i /tmp/log -o /tmp/outlog -c 3 -s 32 -z /tmp/compressed/clog &
	if [ $? -ne 0 ]; then
		print_failed "Failed to start siriusd"
		exit 1
	else
		echo "$SIRIUS started"
	fi
	sleep 1
}

# run after each test case
function teardown {
	kill -9 `pidof "$SIRIUS"`
	sleep 1
	rm -f ${LOGS}* $SIRIUS_PIPE $COMPRESSED_DIR/*
	echo "$SIRIUS stopped"
}

function check_result {
	cd "$COMPRESSED_DIR" || exit
	rm mylog

	no_of_archives=$(ls | wc -l)
	no_of_archives=$((no_of_archives - 1))
	
	for ((i=0; i<=no_of_archives; i++))
	do 
		zlib-flate -uncompress < "${COMPRESSED_DIR}"/"${COMPRESSED_FILE}".${i} >> mylog 
	done
	
	cat ${LOGS}* >> mylog
	expected_hash=`sha256sum -b mylog | head -c 64`
	cd - > /dev/null
	
	echo $expected_hash
}

function test_start_stop {
	setup
	teardown
	print_passed "test_start_stop"
}

function test_simple_message {
	setup

	message="The quick brown fox jumps over the lazy dog.\nThe quick brown fox jumps over the lazy dog.\nThe quick brown fox jumps over the lazy dog."
	echo "$message" > $SIRIUS_PIPE
	sleep 1
	logged_message=`cat ${LOGS}0`
	
	teardown

	if [ "$message" = "$logged_message" ]; then
		print_passed "test_simple_message"
	else
		print_failed "test_simple_message"
	fi
}

function test_more_complex_message_128kb {
	setup

	dd if=/dev/zero of=/tmp/complex_message_128kb bs=131072 count=1
	cat /tmp/complex_message_128kb > $SIRIUS_PIPE
	sleep 5
	
	orig_hash=`sha256sum -b /tmp/complex_message_128kb | head -c 64`
	expected_hash=$(check_result)	
	
	teardown
	rm /tmp/complex_message_128kb

	# it will fail if logs are overwritten
	if [ "$orig_hash" = "$expected_hash" ]; then
		print_passed "test_more_complex_message_128kb"
	else
		print_failed "test_more_complex_message_128kb"
	fi
}

function test_more_complex_message_5mb {
	setup

	dd if=/dev/zero of=/tmp/complex_message_5mb bs=5M count=1
	cat /tmp/complex_message_5mb > $SIRIUS_PIPE
	sleep 5
	
	orig_hash=`sha256sum -b /tmp/complex_message_5mb | head -c 64`
	expected_hash=$(check_result)
	
	teardown
	rm /tmp/complex_message_5mb

	if [ "$orig_hash" = "$expected_hash" ]; then
		print_passed "test_more_complex_message"
	else
		print_failed "test_more_complex_message"
	fi
}

function test_big_file {
	setup

	dd if=/dev/zero of=/tmp/big_file bs=1M count=$1
	cat /tmp/big_file > $SIRIUS_PIPE
	sleep 5
	
	orig_hash=`sha256sum -b /tmp/big_file | head -c 64`
	expected_hash=$(check_result)
	
	teardown
	rm /tmp/big_file 

	if [ "$orig_hash" = "$expected_hash" ]; then
		print_passed "test_more_complex_message"
	else
		print_failed "test_more_complex_message"
	fi
}

echo "*** test_start_stop ***"
test_start_stop
echo "=============================================================================="

echo "*** test_simple_message ***"
test_simple_message
echo "=============================================================================="

echo "*** test_more_complex_message_128kb ***"
test_more_complex_message_128kb
echo "=============================================================================="

echo "*** test_more_complex_message_5mb ***"
test_more_complex_message_5mb
echo "=============================================================================="

echo "*** test_big_file ***"
test_big_file 100
echo "=============================================================================="