/*
 * sirius.c
 *
 *  Created on: Oct 26, 2016
 *      Author: Flavius Lazar
 */

#include <fcntl.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <zlib.h>
#include <argp.h>


#define CHUNK 262144

typedef struct {
	char* input_file;
	char* output_file;
	long max_file_size;
	int current_file_size;
	int no_of_files;

	int next_index;
	int current_fd;
	char* current_output_file;

	char* compressed_file;
	int compressed_index;

} log_info_t;

static void create_pipe(log_info_t* log_info) {
	if (unlink(log_info->input_file) < 0 && errno != ENOENT) {
		perror("Error removing previous log pipe");
		exit(1);
	}

	if (mkfifo(log_info->input_file, 0666) < 0) {
		perror("Error creating log pipe");
		exit(1);
	}
}

static void cleanup(log_info_t* log_info) {
	// if it has assigned a previous name, free it before reuse
	if (log_info->current_output_file != NULL) {
		free(log_info->current_output_file);
	}

	if (log_info->current_fd != -1 && close(log_info->current_fd) < 0) {
		perror("Error closing file descriptor for log file");
		exit(1);
	}
}

/*
 * Computes and returns the length of the log file name.
 * It has to determine the number of digits of the next log file index.
 * length = predefined name + 1 ('.') + no-of-digits + 1 ('/0')
 */
static int get_filename_length(const char* filename, int suffix) {
	int digits = 0;

	do {
		suffix /= 10;
		digits++;
	} while (suffix != 0);

	return (strlen(filename) + digits + 2);
}

/*
 * Read from rd open file descriptor, compress the content and
 * write it to wd open file descriptor.
 * Close rd at the end.
 */
static int compress_file(int rd, int wd, z_stream* zstream, int end) {
	int count = 0;
	int flush;
	int ret;
	unsigned char input[CHUNK];
	unsigned char output[CHUNK];

	while ((count = read(rd, input, CHUNK)) >= 0) {
		zstream->next_in = input;
		zstream->avail_in = count;

		if (count == 0 && end) {
			flush = Z_FINISH;
		}
		else {
			flush = Z_NO_FLUSH;
		}

		do {
			zstream->avail_out = CHUNK;
			zstream->next_out = output;
			ret = deflate(zstream, flush);
			if (ret == Z_STREAM_ERROR) {
				perror("Error during deflate");
				exit(1);
			}

			if (write(wd, output, CHUNK - zstream->avail_out) < 0) {
				perror("Error writing compressed output");
				exit(1);
			}
		} while (zstream->avail_out == 0);

		if (count == 0) {
			break;
		}
	}

	if (count == 0 && end && ret != Z_STREAM_END) {
		perror("Error flushing the compressed file");
		exit(1);
	}

	if (count == -1) {
		perror("Error reading log file");
		exit(1);
	}

	return 0;
}

/*
 * Open a log file.
 */
static int open_log_file(const char* file, int suffix, int flags) {
	int length = get_filename_length(file, suffix);
	char filename[length + 1];
	if (snprintf(filename, length, "%s.%d", file, suffix) < 0) {
		perror("Error preparing the filename");
		exit(1);
	}

	int fd = open(filename, flags, S_IRWXU | S_IRWXG);
	if (fd < 0) {
		perror("Error opening the log file");
		exit(1);
	}

	return fd;
}

void delete_log_file(const char* file, int suffix) {
	int length = get_filename_length(file, suffix);
	char filename[length + 1];
	if (snprintf(filename, length, "%s.%d", file, suffix) < 0) {
		perror("Error preparing the filename");
		exit(1);
	}

	if (unlink(filename) < 0) {
		perror("Error removing log file");
		exit(1);
	}

}

/*
 * Responsible for compression of all existing log files
 */
static int compress_logs(log_info_t* log_info) {
	int ret;
	z_stream zstream;
	int compressed_fd;
	int log_fd;

	zstream.zalloc = Z_NULL;
	zstream.zfree = Z_NULL;
	zstream.opaque = Z_NULL;

	ret = deflateInit(&zstream, Z_DEFAULT_COMPRESSION);
	if (ret != Z_OK) {
		perror("Failed to initialize the compression stream");
		return ret;
	}

	// TODO do compression here
	// open the output compressed file
	compressed_fd = open_log_file(log_info->compressed_file,
			log_info->compressed_index, O_WRONLY | O_CREAT);
	log_info->compressed_index++;
	// iterate through all the existing logs and open the files
	for (int i =0; i < log_info->no_of_files; i++) {
		log_fd = open_log_file(log_info->output_file, i, O_RDONLY);
		compress_file(log_fd, compressed_fd, &zstream, (i == log_info->no_of_files - 1));
		close(log_fd);
		delete_log_file(log_info->output_file, i);

	}

	// final calls to deflate
	do {
		zstream.avail_out = CHUNK;
		ret = deflate(&zstream, Z_FINISH);
		// TODO do I need to write to the output stream?
	} while (zstream.avail_out == 0);

	if (ret != Z_STREAM_END) {
		perror("Failed to finish the compression stream");
		return ret;
	}

	// clean up
	deflateEnd(&zstream);

	// close the compressed file
	close(compressed_fd);

	return 0;
}

void rotate_output_file(log_info_t* log_info) {
	if (log_info->next_index >= log_info->no_of_files) {
		log_info->next_index = 0;
//		printf("Compression should be done now\n");
		compress_logs(log_info);
	}

	int length = get_filename_length(log_info->output_file, log_info->next_index);
	cleanup(log_info);
	log_info->current_output_file = (char*) malloc(length);
	if (log_info->current_output_file == NULL) {
		perror("Error allocating memory for storing output file name");
		exit(1);
	}

	snprintf(log_info->current_output_file, length, "%s.%d",
			log_info->output_file, log_info->next_index);
	// prepare for next time we do the log rotation
	log_info->next_index++;

	log_info->current_fd = open(log_info->current_output_file,
			O_WRONLY | O_TRUNC | O_CREAT, S_IRWXU | S_IRWXG);
	if (log_info->current_fd < 0) {
		perror("Error opening file output log file");
		exit(1);
	}

	// reset file size
	log_info->current_file_size = 0;
//	printf("Logs will be written to %s\n", log_info->current_output_file);
}

static void read_logs(log_info_t* log_info) {
	int fd = open(log_info->input_file, O_RDWR);
	if (fd < 0) {
		perror("Error opening log pipe");
		exit(1);
	}

	struct pollfd fd_set;
	fd_set.fd = fd;
	fd_set.events = POLLIN;
	int ret = 0;
	int READ_BUFFER_SIZE = 2048;
	if (READ_BUFFER_SIZE > log_info->max_file_size) {
		READ_BUFFER_SIZE = log_info->max_file_size;
	}

	char buf[READ_BUFFER_SIZE];
	int num_read = 0;

	// init file rotate mechanism
	rotate_output_file(log_info);

	while (1) {
		// wait until data is available on pipe or timeout
		do {
			ret = poll(&fd_set, 1, 30000);
		} while (ret < 0 && errno == EINTR);

		// if data is available
		if (fd_set.revents != 0) {
			// read it
			if ((num_read = read(fd, buf, READ_BUFFER_SIZE)) < 0) {
				perror("Error reading from log pipe");
				exit(1);
			}
			else {
				// and print it
				buf[num_read] = '\0';
//				printf("%s", buf);

				if (log_info->current_file_size + num_read <= log_info->max_file_size) {
					// current log file can eat this whole chunk
					if (write(log_info->current_fd, buf, num_read) < 0) {
						perror("Error writing logs to file");
						exit(1);
					}
					// update current log file size
					log_info->current_file_size += num_read;
					// TODO what if write() writes less that num_read !?!?
				}
				else if (num_read > 0) {
					// there's not enough room for the whole chunk
					// split it
					int free_space = log_info->max_file_size - log_info->current_file_size;
					if (write(log_info->current_fd, buf, free_space) < 0) {
						perror("Error writing first part of the chunk to log file");
						exit(1);
					}
					// doesn't make any sense to update file size here
					// rotate_output_file with reset it anyway

					rotate_output_file(log_info);

					char chunk[READ_BUFFER_SIZE];
					int length = num_read - free_space;
					strncpy(chunk, buf + free_space, length);
					chunk[length] = '\0';
					if (write(log_info->current_fd, chunk, length) < 0) {
						perror("Error writing second part of the chunk to log file");
						exit(1);
					}
					// update current log file size
					log_info->current_file_size += length;
				}

			}
		}
	}
}

static error_t parse_option(int key, char *arg, struct argp_state *state) {
	log_info_t *log_info = state->input;

	switch (key) {
	case 'i':
		log_info->input_file = arg;
		break;

	case 'o':
		log_info->output_file = arg;
		break;

	case 'c':
		log_info->no_of_files = atoi(arg);
		break;

	case 's':
		// in KB
		log_info->max_file_size = atoi(arg) * 1024;
		break;

	case 'z':
		log_info->compressed_file = arg;
		break;

	case ARGP_KEY_END:
		if (log_info->input_file == NULL) {
			fprintf(stderr, "Must specify named pipe to read logs from!\n");
			argp_usage(state);
		}

		if (log_info->output_file == NULL) {
			fprintf(stderr, "Must specify file where to save logs\n");
			argp_usage(state);
		}

		if (log_info->no_of_files == 0) {
			fprintf(stderr, "Number of output files cannot be 0\n");
			argp_usage(state);
		}

		if (log_info->max_file_size <= 0) {
			fprintf(stderr, "Invalid value for file size\n");
			argp_usage(state);
		}

		if (log_info->compressed_file == NULL) {
			fprintf(stderr, "Must specify where to save compressed logs\n");
			argp_usage(state);
		}
		break;

	default:
		return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

int main(int argc, char **argv) {
	char doc[] = "Logging program capable of compressing high amount of text";
	struct argp_option options[] = {
			{ "in-pipe", 'i', "FILE", 0, "Named pipe from which input is read" },
			{ "out", 'o', "FILE", 0, "Base name for output log files" },
			{ "count", 'c', "NUMBER", 0, "Maximum number of output files to write" },
			{ "size", 's', "NUMBER", 0, "Maximum size in MB for each output file" },
			{ "compress", 'z', "FILE", 0, "Base name for compressed files" },
			{ 0 }
	};

	struct argp argp = { options, parse_option, 0, doc };

	log_info_t log_info;
	log_info.input_file = NULL;
	log_info.output_file = NULL;
	log_info.no_of_files = 0;
	log_info.next_index = 0;
	log_info.current_fd = -1;
	log_info.current_output_file = NULL;
	log_info.max_file_size = 0;
	log_info.current_file_size = 0;
	log_info.compressed_file = NULL;
	log_info.compressed_index = 0;

	argp_parse(&argp, argc, argv, 0, 0, &log_info);
	create_pipe(&log_info);
	read_logs(&log_info);

	return 0;
}
